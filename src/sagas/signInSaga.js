import { takeLatest, put, call } from 'redux-saga/effects';
import {
  GET_AUTHORIZATION,
  SIGN_IN_WITH_GOOGLE,
  SIGN_IN_WITH_VK,
} from '../redux/types';
import {failureAuth, getAuthToken, successAuth} from '../redux/actions';
import firebase from 'firebase/compat';
import { GoogleAuthProvider, signInWithPopup, getAuth } from 'firebase/auth';
import env from 'react-dotenv';

export function* userAuthSagaWatcher() {
  yield takeLatest(GET_AUTHORIZATION, userAuthSagaWorker);
}

function* userAuthSagaWorker({ payload }) {
  try {
    yield call(getSignIn, payload); //выводить успешную авторизацию на экран
    yield put(getAuthToken(firebase.auth().currentUser.uid))
    yield put(successAuth());
  } catch (e) {
    console.log(e, 'eeeeeeeeeeeeeeee');
    yield put(failureAuth(e.code));
  }
}

function getSignIn({ email, password }) {
  console.log("запрос на авторизацию")
  return firebase.auth().signInWithEmailAndPassword(email, password);
}

export function* userGoogleAuth() {
  yield takeLatest(SIGN_IN_WITH_GOOGLE, userGoogleAuthWorker);
}

function* userGoogleAuthWorker() {
  try {
    const req = yield call(getGoogleSignIn);
    console.log(req);
    yield put(successAuth());
  } catch (e) {
    console.log(e);
    yield put(failureAuth(e.code));
  }
}

function getGoogleSignIn() {
  const auth = getAuth();
  const provider = new GoogleAuthProvider();
  return signInWithPopup(auth, provider);
}

export function* userVkAuth() {
  yield takeLatest(SIGN_IN_WITH_VK, userVkAuthWorker);
}

function* userVkAuthWorker() {
  try {
    const req = yield call(getVkSignIn);
    console.log(req);
    yield put(successAuth());
  } catch (e) {
    console.log(e,"error VK");
    yield put(failureAuth(e.code));
  }
}

function getVkSignIn() {
  console.log(
    `${process.env.REACT_APP_VK_AUTH}client_id=${process.env.REACT_APP_ID}&display=popup&redirect_uri=${process.env.REACT_APP_REDIRECT}&response_type=code&v=5.52`,
  );
  return fetch(
    `https://oauth.vk.com/authorize?client_id=7957443&display=popup&redirect_uri=https://users-data-556ed.firebaseapp.com/signIn&scope=email+offline&response_type=code&v=5.52`,
    {
      method: 'POST',
      headers: {
        "Access-Control-Allow-Origin": "*",
        "Connection": "Keep-Alive",
        "Transfer-Encoding": "chunked",
        "Content-Type": "application/xml",
      }
    },
  ).then(response => {
    console.log(response);
    return response.json();
  });
}
//
