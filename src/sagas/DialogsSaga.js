import { call, put, take, takeLatest } from 'redux-saga/effects';
import {
  FETCH_ACTIVE_DIALOGS,
  GET_MESSAGES,
  SAVE_DIALOG,
  SEARCH_DIALOG,
  SEND_MESSAGE,
  TOGGLE_CHAT,
} from '../redux/types';
import firebase from "firebase/compat";
import { getActive, getMessageSuccess } from '../redux/actions';
import { eventChannel, END} from 'redux-saga';
import { getMessage } from '@testing-library/jest-dom/dist/utils';


export function* searchDialog () {
  yield takeLatest(SEARCH_DIALOG, searchDialogWorker)
}

function* searchDialogWorker({payload}) {
  try {
    const mes = yield call(search);
    console.log(mes, "from saga worker");
  }
  catch (error) {
    console.log(error, "from DIALOGSEARCH");
  }
}

function search() {
  const db = firebase.database();
  const ref = db.ref('/dialogs');
  return ref.orderByChild("messages/0/name").equalTo("andrew petrov").once("value").then((snapshot) => (
    snapshot.val()
  ))
  // console.log(message)
  // const db = getDatabase();
  // const dfRef = query(ref(db, 'active/1uh3u1h2312hu/messages'));
  // return onValue(dfRef, (snapshot) => {
  //  let data = snapshot.val();
  //  return data;
  // })
}



export function* getDialogs() {
  yield takeLatest(FETCH_ACTIVE_DIALOGS,getDialogWorker);
}

function* getDialogWorker({ payload }) {
  let channel = yield call(fetchChannel, payload);
  //console.log(channel)
    //console.log(payload)
    while(true) {
      const dialogs = yield take(channel);
      yield put(getActive(Object.values(dialogs), Object.keys(dialogs)));
    }
    // const dialogs = yield new Promise((resolve) =>
    //   firebase.database().ref('/dialogs').orderByChild("uid").equalTo("new").on("value", resolve))
    // console.log("dialogs??????",dialogs)
    //let length = Object.keys(data)[Object.keys(data).length - 1];
    //let length = Object.keys(data).length;
    //let newdata = Object.values(data)
    //console.log(newdata)
    //Object.setPrototypeOf(data, Array(length));
    //yield put(getActive(Object.values(data)))
     // : yield  put(getActive([data/*Object.values(data)[0]*/]))

}

function fetchChannel (data) {
  console.log(data,"FETCHCHANNEL")
  const ref = firebase.database().ref('/dialogs');
  return eventChannel( emitter => {
      ref.orderByChild("uid").equalTo("new").on("value",snapshot => {
        if(snapshot.val()) emitter(snapshot.val())
      });
    return () => {
      //emitter(END);
    }
  })
//
//
//   // if(status !== "new") {
//   //   return ref.orderByChild(status === "saved" ? "1uh3u1h2312hu/saved" : "1uh3u1h2312hu/status").equalTo(status === "saved" ? true : status).once("value").then((snapshot) =>(
//   //     snapshot.val()
//   //   ))
//   // }
//   // return ref.orderByChild("uid").equalTo("new").on("value",   snapshot => {
//   //   return (snapshot) => (
//   //     snapshot.val()
//   //   )
//   // })
}


export function* saveDialog() {
  yield takeLatest(SAVE_DIALOG, saveDialogWorker)
}

function* saveDialogWorker({payload}) {
  try {
    const res = yield call(fetchSave, payload)
    console.log(res)
  }
  catch(error) {
    console.log(error)
  }
}

function fetchSave({dialogId, saved} ) {
  const db = firebase.database();
  const ref = db.ref("dialogs");
  const updateData = {
    "saved": saved
  }
  //return ref.child(`${dialogId}/1uh3u1h2312hu`).update(updateData)
  return ref.child(`1uh3u1h2312hu/`)
}


export function* openChatWatcher () {
  yield takeLatest(TOGGLE_CHAT, openChatWorker)
}

function* openChatWorker ({ payload }) {
  try{
    yield firebase.database().ref(`dialogs/${payload.dialogId}`).update({
      isQueue : false,
      chatStarted: payload.toggle,
      uid : {
        [payload.uid]: {
          saved : false,
          status : "active"
        },
      }
    })
  }
  catch (err) {
    console.log(err)
  }
}

export function* sendMessage() {
  yield takeLatest(SEND_MESSAGE, sendMessageWorker)
}
function* sendMessageWorker({payload}) {
  try{
    yield firebase.database().ref(`/dialogs/${payload.dialogId}/messages`).push({
      content: payload.message,
      timeStamp: Date.now(),
      writtenBy: "operator"
    })
  }
  catch (error) {
    console.log(error)
  }
}

// export function* getMessages() {
//   yield takeLatest(GET_MESSAGES, getMessagesWorker)
// }
// function* getMessagesWorker(data) {
//   let channel = yield call(fetchChannel, data.payload)
//   try{
//       while (true) {
//         const messages = yield take(channel);
//         yield put(getMessageSuccess(messages));
//       }
//   }
//   catch (error){
//     console.log(error)
//   }
// }
// function fetchMessages(dialogId) {
//   return eventChannel( emitter => {
//     firebase.database().ref(`/dialogs/${dialogId}/messages`).on("value", snapshot => {
//       const data = {
//         key: Object.keys(snapshot.val()),
//         messages: Object.values(snapshot.val())
//       }
//       console.log(data)
//       emitter(data)
//     })
//     return () => {
//       emitter(END)
//     }
//   })
// }