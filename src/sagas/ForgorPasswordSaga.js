import { call, put, takeLatest } from 'redux-saga/effects';
import { CHANGE_PASSWORD, UPDATE_PASSWORD } from '../redux/types';
import {
  getAuth,
  sendPasswordResetEmail,
  confirmPasswordReset,
} from 'firebase/auth';
import {failureAuth, successAuth} from '../redux/actions';

export function* changePassword() {
  yield takeLatest(CHANGE_PASSWORD, changePasswordWorker);
}

function* changePasswordWorker({ payload }) {
  try {
    const req = yield call(getChangePassCode, payload);
    console.log(req);
    yield put(successAuth());
  } catch (e) {
    console.log(e);
    yield put(failureAuth(e.code))
  }
}

function getChangePassCode({ email }) {
  const actionCodeSettings = {
    url: 'https://users-data-556ed.firebaseapp.com/updatePassword',
    handleCodeInApp: true,
  };
  const auth = getAuth();
  return sendPasswordResetEmail(auth, email);
  //ActionCodeOperation.PASSWORD_RESET
  //sendPasswordResetEmail(auth, email, actionCodeSettings);
  //return firebase.auth().sendPasswordResetEmail(email)
}

export function* updatePassword() {
  yield takeLatest(UPDATE_PASSWORD, updatePasswordWorker);
}

function* updatePasswordWorker({payload}) {
  try {
    const req = yield call(getUpdatePassword, payload);
    console.log('req=========>', req);
    yield put(successAuth());
  } catch (e) {
    console.log(e);
  }
}

function getUpdatePassword({ oobCode, password }) {
  const auth = getAuth();
  return confirmPasswordReset(auth, oobCode, password);
}
