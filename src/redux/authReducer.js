import {
  AUTHORIZATION_SUCCESS,
  AUTHORIZATION_FAILURE,
  GET_AUTHORIZATION,
  GET_SIGN_OUT,
  GET_REGISTRATION,
  CHANGE_PASSWORD,
  UPDATE_PASSWORD,
  SIGN_IN_WITH_GOOGLE,
  SIGN_IN_WITH_VK,
  GET_USER_TOKEN,
  SEARCH_DIALOG,
  FETCH_ACTIVE_DIALOGS,
  GET_ACTIVE_DIALOGS,
  SAVE_DIALOG,
  DELETE_SAVE,
  OPEN_CHAT,
  TOGGLE_CHAT,
  SEND_MESSAGE,
  GET_MESSAGES, GET_MESSAGES_SUCCESS,
} from './types';

const initialState = {
  userData: {},
  isLoading: false,
  userAuth: false,
  errorMessage: "",
  userToken: "",
  message: "",
  searchedMessage: "",
  activeDialog: [],
  dialogId: "",
  dialogLoaded: false,
  status: "",
  chatMessages: [],
  messageKey: [],
  chatIsOpen: false
};

export const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case UPDATE_PASSWORD:
      return { ...state, isLoading: true, userData: action.payload };
    case CHANGE_PASSWORD:
      return { ...state, isLoading: true, userData: action.payload };
    case GET_SIGN_OUT:
      return { ...state, isLoading: false, userAuth: false };
    case GET_REGISTRATION:
      return { ...state, isLoading: true, userData: action.payload };
    case AUTHORIZATION_SUCCESS:
      return { ...state, isLoading: false, userAuth: true, userData: {} };
    case AUTHORIZATION_FAILURE:
      return {...state, isLoading: false, userAuth: false, errorMessage: action.payload};
    case GET_AUTHORIZATION:
      return { ...state, isLoading: true, userData: action.payload };
    case SIGN_IN_WITH_GOOGLE:
      return { ...state, isLoading: true };
    case SIGN_IN_WITH_VK:
      return { ...state, isLoading: true };
    case GET_USER_TOKEN:
      return {...state, userToken: action.payload };
    case SEARCH_DIALOG:
      return {...state, message: action.payload };
    case FETCH_ACTIVE_DIALOGS:
      return { ...state, dialogLoaded: true, status: action.payload}
    case GET_ACTIVE_DIALOGS:
      return { ...state, activeDialog: action.data , dialogId: action.payload}
    case SAVE_DIALOG:
      return {...state,}
    case DELETE_SAVE:
      return {...state, activeDialog: action.payload}
    case OPEN_CHAT:
      return {...state, chatMessages: action.payload}
    case TOGGLE_CHAT:
      return  {
        ...state,
        chatIsOpen: action.payload.toggle,
        dialogId: action.payload.dialogId
      }
    case SEND_MESSAGE:
      return { ...state}
    case GET_MESSAGES:
      return {...state};
    case GET_MESSAGES_SUCCESS:
      return {
        ...state,
        chatMessages: action.payload.messages,
        messageKey: action.payload.key
      }
    default:
      return state;
  }
};
