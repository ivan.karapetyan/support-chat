import React, {useEffect, useState} from 'react';
import { Spinner} from "reactstrap";
import {useDispatch,connect} from "react-redux";
import DialogCard from "./DialogCard";
import {deleteSaveDialog, fetchActive, fetchDialog} from "../redux/actions";
import firebase from 'firebase/compat';



const dialogList = {
  width: "100%",
  height: "100%",
  padding: "20px",
}
const dialogList__wrapper = {
  height: "100%",
  background: "#3596ff",
  border: "2px solid #3596ff",
  padding: "20px",
  borderRadius: "12px",
  overflow: "auto"
}

const spinner = {
  width: "100px",
  height: "100px",
  margin: "180px 450px",
}


const DialogsList = ({activeDialog, status, userToken, dialogId}) => {
  const [count, setCount] = useState(0)
  const dispatch = useDispatch();

  useEffect( () => {
    if(status === "new"){
      setCount(activeDialog.filter( item => item ).length)
    }
  }, [activeDialog])

  function deleteSave (dialog) {
    let newDialog = activeDialog.filter( (item, index) => index !== dialog);
    dispatch(deleteSaveDialog(newDialog));
  }
  console.log("DialogId",dialogId)
  console.log("userToken===>",userToken)
  React.useEffect( () => {
    console.log(activeDialog, "======> стейт диалогов")
    if(userToken){
      dispatch(fetchDialog("new", userToken));
    }
  },[userToken])
  if(activeDialog.length < 1) {
    return <Spinner style={spinner} color="primary" type="grow" />
  }
  return (
    <div style={dialogList}>
      <div style={dialogList__wrapper}>
        {status === "new" && (<div>Клиентов в очереди: {count}</div>)}
        {activeDialog.map( (item, index) => {
            return <DialogCard key={item.dialog_id}
                        dialog={item}
                        dialogId={dialogId[index]}
                        deleteSave={deleteSave}
            />
          }
        )}
      </div>
    </div>
  );
};

const mapStateToProps = state => ({
  activeDialog: state.activeDialog,
  status: state.status,
  userToken: state.userToken,
  dialogId: state.dialogId,
})
export default connect(mapStateToProps, null)(DialogsList);