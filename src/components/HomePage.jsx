import React from 'react';
import Header from './Header';
import MenuBar from './MenuBar';
import ChatArea from "./ChatArea";
import DialogsList from "./DialogsList";
import {connect} from "react-redux";

const HomePage = ({chatIsOpen}) => {
  const wrapper = {
    display: "flex",
    flexDirection: "column",
    height: "100vh"
  }

  return (
    <>
      <MenuBar/>
      <div style={wrapper}>
        <Header />
        {chatIsOpen ? <ChatArea/> : <DialogsList/>}
      </div>
    </>
  );
};

const mapStateToProps = state => ({
  chatIsOpen: state.chatIsOpen
})

export default connect(mapStateToProps, null)(HomePage);
