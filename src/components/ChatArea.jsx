import React, { useEffect, useState } from 'react';
import Loading from "./Loading";
import {Input, Spinner} from "reactstrap";
import firebase from "firebase/compat";
import { connect, useDispatch } from 'react-redux';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { fas, faPaperPlane } from '@fortawesome/free-solid-svg-icons';
import Moment from 'react-moment';
import { getMessages, getMessagesChat, getMessageSuccess, sendMessage } from '../redux/actions';

const dialogList = {
  width: "100%",
  height: "92%",
}
const dialogList__wrapper = {
  display: "flex",
  flexDirection: "column",
  height: "100%",
  background: "#f7f9fa",
  overflow: "hidden",
  position: "relative",
  justifyContent: "flex-end",
  paddingBottom: "70px"
}
const chatArea__message_card = {
  display: "flex",
  flex: "1",
  alignSelf: "flex-start",
  chatArea__card_left: {
    alignSelf: "flex-end"
  }
}
const chatArea__user = {
  display: "flex",
  flexDirection: "column",
  justifyContent: "flex-end",
}
const chatArea__pict = {
  height: "46px",
  width: "46px",
  borderRadius: "50%",
}
const chatArea__message = {
  forAll: {
    padding: "20px",
    marginBottom: "50px",
    maxWidth: "600px",
  },
  operator: {
    background: "#007aff",
    borderRadius: "12px 12px 0 12px",
  },
  client: {
    background: "#eaeaea",
    borderRadius: "12px 12px 12px 0",
  }
}
const input__wrapper = {
  position: "absolute",
  bottom: "70px",
  width: "80%",
}
const input = {
  position: 'absolute',
  height: "70px",
  boxShadow: "none",
  background: "#ebebeb",
  padding: "30px 25px",
  fontSize: "18px",
  color: "#a1a1a1"
}
const send_message = {
  position: "absolute",
  right: "20px",
  top: "16px",
  height: "40px",
  width: "40px",
  borderRadius: "50%",
  background: "#007aff",
  padding: "7px 10px"

}
const spinner = {
  width: "100px",
  height: "100px",
  margin: "180px 450px",
}

const ChatArea = ({chatMessages, dialogId, messageKey}) => {
  const [message, setMessage] = React.useState("");
  const dispatch = useDispatch();
  console.log("chatMessages===>",chatMessages,messageKey)
  console.log(message)

  React.useEffect(() => {
    if(dialogId) {
      firebase.database().ref(`/dialogs/${dialogId}/messages`).on("value", snapshot => {
        const data = {
          key: Object.keys(snapshot.val()),
          messages: Object.values(snapshot.val())
        }
        dispatch(getMessageSuccess(data))
      });
    }
  },[]);

  if(chatMessages.length < 1) {
    return <Spinner style={spinner} color="primary" type="grow" />
  }

  const handleSend = (isEnter) => {
    if(isEnter.type === "click" || isEnter === "Enter" || isEnter === "NumpadEnter"){
      dispatch(sendMessage(dialogId,message));
      setMessage('');
    }
  }

  return (
    <div style={dialogList}>
      <div style={dialogList__wrapper}>
        {
          chatMessages.map( (item, index) => {
            if (item.writtenBy === "client") {
              return  <div key={messageKey[index]} style={chatArea__message_card}>
                        <div style={chatArea__user}>
                          <img
                            style={chatArea__pict}
                            alt="Изображение профиля"
                            src="https://cdn1.savepice.ru/uploads/2021/9/27/2feef6f4de8bbe9c241a67997f69d049-full.jpg"
                          />
                          <div>{item.timestamp}</div>
                        </div>
                        <div style={{...chatArea__message.forAll, ...chatArea__message.client}}>
                          {item.content}
                        </div>
                      </div>
            }
            else{
              return <div key={messageKey[index]} style={{...chatArea__message_card, ...chatArea__message_card.chatArea__card_left}}>
                        <div style={{...chatArea__message.forAll, ...chatArea__message.operator}}>
                          {item.content}
                        </div>
                        <div style={chatArea__user}>
                          <img
                            style={chatArea__pict}
                            alt="Изображение профиля"
                            src="https://cdn1.savepice.ru/uploads/2021/9/27/2feef6f4de8bbe9c241a67997f69d049-full.jpg"
                          />
                          <div>
                            <Moment locale="ru" unix>
                              {item.timestamp}
                            </Moment>
                          </div>
                        </div>
                      </div>
            }
          })
        }
        <div style={input__wrapper}>
          <Input
            style={input} placeholder="Напишите сообщение..."
            onKeyPress={(event)=> handleSend(event.code)}
            onChange={(event => setMessage(event.target.value))}
          />
          <span className="send-message">
            <FontAwesomeIcon
              style={{color: "white"}}
              icon={faPaperPlane}
              onClick={handleSend}

            />
          </span>
          <span className="input__stickers"> </span>
          <span className="input__clip"> </span>
        </div>
      </div>
    </div>
  );
};

const mapStateToProps = state => ({
  chatMessages: state.chatMessages,
  dialogId: state.dialogId,
  messageKey: state.messageKey,
})

export default connect(mapStateToProps, null)(ChatArea);