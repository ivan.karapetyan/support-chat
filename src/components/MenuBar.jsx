import React from 'react';
import Dialogs from "./Dialogs";
import {Input, Label} from "reactstrap";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch} from "@fortawesome/free-solid-svg-icons";
import debounce from 'lodash.debounce';
import {useDispatch, connect} from "react-redux";
import {fetchDialog, searchDialog, toggleChat} from "../redux/actions";

const MenuBar = ({status}) => {
  const [hover, setHover] = React.useState(false);
  const [logoHover, setLogoHover] = React.useState(false)
  const dispatch = useDispatch();
  const menuBar = {
    //width: "fit-content",
    display: "flex",
    flexDirection: "column",
    background: '#007aff',
    height: '100vh',
    borderRadius: '0 10px 10px 0',
    float: "left",
    padding: "10px 0",
    //position: 'absolute',
  };
  const search = {
    margin: "10px 15px",
    background: "#3596ff",
    color: "white",
    paddingLeft: "35px",
    width: "250px" ,
    border: "none",
  }
  const search__icon = {
    color: "#9bcbff",
    position: "absolute",
    top: "20px",
    left: "27px",
  }

  const dialogsData = ["Активные", "Завершенные", "Сохраненные"];

  const [type, setType] = React.useState(9999)
  function handleChange(radioKey) {
    setType(radioKey);
  }

  const handleSearchChange = debounce(event => dispatch(searchDialog(event.target.value)),1000);


  return (
    <div style={menuBar}>
      <h1
        style={{color: "white", textAlign: "center", padding: "10px", background: logoHover ? "#3596ff" : null, cursor: "pointer"}}
        onMouseEnter={() => setLogoHover(true)}
        onMouseLeave={() => setLogoHover(false)}
        onClick={() => status === "new" ? null : ( dispatch(fetchDialog("new")), setType(9999)) }
      >
          WEHELP
      </h1>
      <Label style={{position: "relative", background: hover ? "#3596ff" : null}} >
        <Input
          id="search"
          type="text"
          placeholder="Поиск диалогов"
          style={search}
          onMouseEnter={() => setHover(true)}
          onMouseLeave={() => setHover(false)}
          onChange={(event) => handleSearchChange(event)}
        />
        <FontAwesomeIcon
          icon={faSearch}
          style={search__icon}
        />
      </Label>

      {dialogsData.map((item,index) => (
        <Dialogs
          type={type}
          handleChange={handleChange}
          index={index}
        >
          {item}
        </Dialogs>
      ))}

    </div>
  )
};

const mapStateToProps = state => ({
  status: state.status
})

export default connect(mapStateToProps, null)(MenuBar);
