import React from 'react';
import { Link } from 'react-router-dom';
import {Button} from 'reactstrap'

const AuthLinks = ({ children }, props) => {
  const signIn__linkWrapper = {
    display: 'flex',
    justifyContent: 'space-between',
    margin: '15px 0 0',
  };
  const signIn__reglink = {
    color: '#007aff',
    justifyItems: 'right',
  };

  return (
    <div style={signIn__linkWrapper}>
      <Link
        to={children.frsLink === 'Войти' ? '/signIn' : '/registration'}
        style={{ textDecoration: 'none' }}
      >
        <Button color="link" style={signIn__reglink}>{children.frsLink}</Button>
      </Link>
      <Link
        to={
          children.scnLink === 'Регистрация'
            ? '/registration'
            : '/forgotPassword'
        }
        style={{ textDecoration: 'none' }}
      >
        <Button color="link" style={signIn__reglink}>{children.scnLink}</Button>
      </Link>
    </div>
  );
};

export default AuthLinks;
