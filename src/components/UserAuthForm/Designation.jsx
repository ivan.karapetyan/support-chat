import React from 'react';

const Designation = ({ children }) => {
  const designation = {
    color: '#007aff',
    margin: '0 0 20px',
    textAlign: 'center'
  };

  return <h1 style={designation}>{children}</h1>;
};

export default Designation;
