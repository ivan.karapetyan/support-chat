import React from 'react';
import { Route, Switch } from 'react-router-dom';
import HomePage from './HomePage';
import SignIn from './SignIn';
import Registration from './Registration';
import ForgotPass from './ForgotPass';
import UpdatePassword from './UpdatePassword';
import ChatArea from "./ChatArea";

const AppRouter = () => {
  return (
    <Switch>
      <Route exact path="/" component={HomePage} />
      <Route path="/signIn" component={SignIn} />
      <Route path="/registration" component={Registration} />
      <Route path="/forgotPassword" component={ForgotPass} />
      <Route path="/updatePassword" component={UpdatePassword} />
      {/*<Route path="/chat" component={ChatArea} />*/}
    </Switch>
  );
};

export default AppRouter;
