import React, {useEffect} from 'react';
import AuthForm from './UserAuthForm/AuthForm';
import Designation from './UserAuthForm/Designation';
import TextField from './UserAuthForm/TextField';
//import Button from './UserAuthForm/Button';
import {Button} from 'reactstrap'
import AuthLinks from './UserAuthForm/AuthLinks';
import { useLocation } from 'react-router';
import { useFormik } from 'formik';
import { useDispatch, connect } from 'react-redux';
import { updatePassword } from '../redux/actions';
import { toast, ToastContainer } from 'react-toastify';
import Loading from './Loading';

const validate = values => {
  const errors = {};
  if (!values.password) {
    errors.password = 'Введите пароль';
  } else if (
    !/(?=.*[0-9])(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z])/g.test(values.password)
  ) {
    errors.password =
      'Пароль должен содержать цифру, буквы в нижнем и верхнем регистре';
  } else if (values.password.length > 20 || values.password.length < 8) {
    errors.password =
      values.password.length > 20
        ? 'Must be 20 characters or less'
        : 'Must be 8 characters or more';
  }

  if (!values.passwordConfirm) {
    errors.passwordConfirm = 'Required';
  } else if (values.password !== values.passwordConfirm) {
    errors.passwordConfirm = 'Пароли не совпадают!';
  }
  return errors;
};

const UpdatePassword = ({ isLoading, userAuth, history, errorMessage }) => {
  let oob = new URLSearchParams(useLocation().search);
  const dispatch = useDispatch();
  const formik = useFormik({
    initialValues: {
      password: '',
      passwordConfirm: '',
    },
    validate,
    onSubmit: values => {
      console.log('oob ================>', oob.get('oobCode'));
      let data = {
        oobCode: oob.get('oobCode'),
        password: values.password,
      };
      dispatch(updatePassword(data));
    },
  });

  const signIn__error = {
    color: 'red',
    marginLeft: '15px',
    fontWeight: '600',
    position: 'absolute',
    right: '5px',
  };

  toast.success('Пароль успешно обновлен!');

  useEffect(() => {
    if (userAuth) {
      setTimeout(function () {
        history.push('/signIn');
      }, 2000);
    }
  }, [userAuth]);

  return isLoading ? (
    <Loading />
  ) : (
    <AuthForm>
      {userAuth ? (
        <ToastContainer position="top-center" limit={1} autoClose={null} />
      ) : null}
      <Designation>Обновите пароль</Designation>
      <form onSubmit={formik.handleSubmit}>
        <TextField
          id="password"
          label="Пароль"
          name="password"
          type="password"
          onChange={formik.handleChange}
          value={formik.values.password}
        >
          {formik.errors.password ? (
            <span style={signIn__error}>{formik.errors.password}</span>
          ) : null}
        </TextField>
        <TextField
          id="passwordConfirm"
          label="Подтвержение пароля"
          name="passwordConfirm"
          type="password"
          onChange={formik.handleChange}
          values={formik.values.passwordConfirm}
        >
          {formik.errors.passwordConfirm ? (
            <span style={signIn__error}>{formik.errors.passwordConfirm}</span>
          ) : null}
        </TextField>
        <Button color="primary" style={{width: "100%", margin: "15px 0 0"}}>Сменить пароль</Button>
      </form>
      <AuthLinks>{{ frsLink: 'Войти', scnLink: 'Регистрация' }}</AuthLinks>
    </AuthForm>
  );
};

const mapStateToProps = state => ({
  isLoading: state.isLoading,
  userAuth: state.userAuth,
});

export default connect(mapStateToProps, null)(UpdatePassword);
