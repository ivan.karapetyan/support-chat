

const StarRating = ({rating}) => {
  const starRate = [ 1, 2, 3, 4, 5];
  return (
    <div className="star__wrapper">
      {starRate.map( (start, index) => {
        return <span className={index < rating ? "star_rate" : null}> </span>
      })}
    </div>
  );
};

export default StarRating;