import React from 'react';

const Error = ({ children }) => {
  const authError = {
    position: 'absolute',
    left: '550px',
    top: '120px',
    color: 'red',
    fontSize: '22px',
  };
  return <div style={authError}>{children}</div>;
};

export default Error;
